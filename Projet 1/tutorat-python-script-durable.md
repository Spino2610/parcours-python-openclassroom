### Prérequis

* Avoir déja les bases de scripting avec Python

### Intro

Le meilleur script est celui que l'on peut comprendre/modifier sans l'avoir utilisé pendant plus de 6 mois.

La [bibliothèque standard Python](https://docs.python.org/fr/3/library/index.html) comporte tout ce qu'il faut pour y arriver, moyennant relativement peu d'effort un peu d'habitudes.

### Support

Ce tuto utilisera comme base un algorithme déterminant le [plus grand diviseur commun](https://fr.wikipedia.org/wiki/Plus_grand_commun_diviseur_de_nombres_entiers) (PGCD), par _soustractions successives_ :

> Soient a et b deux entiers naturels non nuls avec `a > b`
>
> Alors : `PGCD(a;b)` = `PGCD(b;a−b)`

Cette méthode est chaînable et donc se scripte facilement. Exemple avec la recherche de `PGCD(561;357) `:

1. `561 - 357 = 204`
1. `357 - 204 = 153`
1. `204 - 153 = 51`
1. `153 - 51  = 102`
1. `102 - 51  = 51`
1. `51 - 51   = 0`
1. Donc : `PGCD(561;357) = 51`

### À faire

1. Base
    * [ ] Créer un nouveau dépôt `<user>/tuto_pysdur`
    * [ ] Lire [`docs.gitlab` -- _Closing issues automatically_](https://docs.gitlab.com/ce/user/project/issues/managing_issues.html#closing-issues-automatically)
    * [ ] Lire [`docs.gitlab` -- _Autocomplete characters_](https://docs.gitlab.com/ce/user/project/autocomplete_characters.html)
        * 🔊 Lier chaque commit à ce ticket (_issue_) : minimum un commit par étape (case à cocher)
    * [ ] Lire [_PEP 257 -- Docstring Conventions_](https://www.python.org/dev/peps/pep-0257/)
    * [ ] Décrire clairement l'objectif du script en quelques mots/lignes avec des  [`docstring`](https://docs.python.org/fr/3/glossary.html#term-docstring) en en-tête de [module](https://docs.python.org/fr/3/glossary.html#term-module) et de fonction
1. Code & test
    * [ ] Implémenter [l'algorithme](#support) dans une fonction prenant en paramètre un couple d'entier décroissant, la réponse affiche juste le résultat final en console
    * [ ] Lire [`doctests` -- Test interactive Python examples](https://docs.python.org/fr/3/library/doctest.html) (s'arrêter avant [_Basic API_](https://docs.python.org/fr/3/library/doctest.html#basic-api) devrait suffir)
    * [ ] Ajouter quelques `doctests` pour tester le comportement de la fonction
1. Arguments documentés
    * [ ] Lire le 1er chapitre _Exemples_ de [`argparse`-- Parseur d'arguments, d'options, et de sous-commandes de ligne de commande](https://docs.python.org/fr/3/library/argparse.html)
    * Utiliser le module `argparse` pour ajouter des argument à votre code :
        * [ ] Argument positionnel : une paire d'entier (`a` & `b`)
        * [ ] Argument optionnel (`-v, --verbose` ) affiche la réponse complète : `PGCD(a;b) = c`
        * [ ] Utiliser la docstring du module dans l'affichage de l'aide
1. Journalisation (_logging_)
    * [ ] Lire [_Les bases de l'utilisation du module logging_](https://docs.python.org/fr/3/howto/logging.html#logging-howto)
    * Ajouter des événements à **message statique** du type :
        - [ ] `CRITICAL` : quand les arguments sont donné dans l'ordre croissant
        - [ ] `INFO` : quand la fonction  est appelée
        - [ ] `DEBUG` : à chaque soustraction
    * [ ] Remplacer les messages ci-dessus par des [messages variables](https://docs.python.org/fr/3/howto/logging.html#logging-variable-data)
    * Ajouter un argument qui :
        - [ ] affiche en console tous les types d'évènements (`-d, --debug`)
        - [ ] écrit dans un fichier `.log` (dans le dossier courant) et n'affiche rien en console (`-l, --logfile`)
1. Fin
    * [ ] Supprimer les messages parasite du shell (ex. : `<mon_shell>: exit 2 python3 pgcd.py`)
    * [ ] Proposer une amélioration (au moins) du [modèle de ce tutorat](https://gitlab.com/forga/process/fr/embarquement/-/edit/stable/.gitlab/issue_templates/tutorat-python-script-durable.md)
    * [ ] Clore ce ticket (_issue_)


Suite
-----

Ce ticket (_issue_) terminé, on peut passer à la découverte de :

1. «[_GitLab_ & _Django_](https://gitlab.com/forga/process/fr/embarquement/-/issues/new?issue[title]=Tutorat%20GitLab-Django%20de%20<mon%20nom>&issuable_template=tutorat-gitlab-django)»
1. «[_PlantUML_](https://gitlab.com/forga/process/fr/embarquement/-/issues/new?issue[title]=Tutorat%20PlantUML%20de%20<mon%20nom>&issuable_template=tutorat-puml)»
1. «[_Pytest_/_GitLab CI_](https://gitlab.com/forga/process/fr/embarquement/-/issues/new?issue[title]=Tutorat%20Pytest-Gitlab%20CI%20de%20<mon%20nom>&issuable_template=tutorat-pytest-gitlab_ci)»

---

* 🍻 Merci [_Vincent Bernat_](https://github.com/vincentbernat) pour son blog post qui a complètement inspiré ce tuto ( [🇫🇷](https://vincent.bernat.ch/fr/blog/2019-script-python-durable) | [🇬🇧](https://vincent.bernat.ch/en/blog/2019-sustainable-python-script) ).


<!-- Quick actions GitLab : ne rien écrire d'autre sous cette ligne SVP  -->
/assign me
/cc @all
/due in 1 week
/todo
/label ~"follow::todo"
