# Parcours Python Openclassroom

## Objectif

Le but de ce projet est de pouvoir stocker tous les projets et voir l'avancement du parcour python OpenClassroom et de tester si cela est possible sur GitLab

## Introduction

Utilisation du parcour tutorat de [`freezed`](https://gitlab.com/forga/process/fr/embarquement)

## Liste des projets

`Projet 1:` Démarrez votre formation développeur d'application Python (via [`freezed`](https://gitlab.com/forga/process/fr/embarquement))  

`Projet 2:` Utilisez les bases de Python pour l'analyse de marché  

`Projet 3:` Designez une application Python adaptée aux besoins d'un client  

`Projet 4 :` Développez un programme logiciel en Python

`Projet 5 :` Testez votre maîtrise du langage Python

`Projet 6 :` Développez une interface utilisateur pour une application web Python

`Projet 7 :` Résolvez des problèmes en utilisant des algorithmes en Python

`Projet 8 :` Testez votre maîtrise des algorithmes en Python

`Projet 9 :` Développez une application Web en utilisant Django

`Projet 10 :` Créez une API sécurisée RESTful en utilisant Django REST

`Projet 11 :` Améliorez une application Web Python par des tests et du débogage

`Projet 12 :` Développez une architecture back-end sécurisée en utilisant Django ORM

`Projet 13 :` Mettez à l'échelle une application Django en utilisant une architecture modulaire
